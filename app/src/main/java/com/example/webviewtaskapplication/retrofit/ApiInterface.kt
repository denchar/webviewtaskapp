package com.example.webviewtaskapplication.retrofit

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {


    @GET("test.php")
    fun getPictureOfDay(): Observable<ResponseModel>
}