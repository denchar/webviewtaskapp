package com.example.webviewtaskapplication.ui.webviewactivity


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Parcelable
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.View
import android.webkit.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.webviewtaskapplication.ConnectionService
import com.example.webviewtaskapplication.R
import kotlinx.android.synthetic.main.activity_web_view.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class WebViewActivity : AppCompatActivity() {

    private val url = "https://google.com"
    private var mUploadMessage: ValueCallback<Uri?>? = null
    private var mCapturedImageURI: Uri? = null
    private var mFilePathCallback: ValueCallback<Array<Uri>>? = null
    private var mCameraPhotoPath: String? = null
    private val INPUT_FILE_REQUEST_CODE = 1
    private val FILECHOOSER_RESULTCODE = 1
    private val TAG = WebViewActivity::class.java.simpleName

    companion object {
        fun goToWebViewActivity(context: Context) {
            context.startActivity(Intent(context, WebViewActivity::class.java))
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)

            if (Build.VERSION.SDK_INT >= 23 && (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.CAMERA
                ) != PackageManager.PERMISSION_GRANTED)
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA
                    ),
                    1
                )
            }

            assert(webview != null)

            webview.settings.javaScriptEnabled = true
            webview.settings.allowFileAccess = true
            webview.settings.setSupportZoom(false)

            webview.settings.setAppCacheEnabled(true)
            webview.settings.domStorageEnabled = true
            if (Build.VERSION.SDK_INT >= 21) {
                webview.settings.mixedContentMode = 0
                webview.setLayerType(View.LAYER_TYPE_HARDWARE, null)
            } else if (Build.VERSION.SDK_INT >= 19) {
                webview.setLayerType(View.LAYER_TYPE_HARDWARE, null)
            } else if (Build.VERSION.SDK_INT < 19) {
                webview.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
            }

            webview.webViewClient = Callback()
            webview.webChromeClient = object : WebChromeClient() {

                @SuppressLint("SimpleDateFormat")
                @Throws(IOException::class)
                private fun createImageFile(): File? {
                    val timeStamp =
                        SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
                    val imageFileName = "JPEG_" + timeStamp + "_"
                    val storageDir =
                        Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES
                        )
                    return File.createTempFile(
                        imageFileName,  /* prefix */
                        ".jpg",  /* suffix */
                        storageDir /* directory */
                    )
                }

                override fun onShowFileChooser(
                    view: WebView,
                    filePath: ValueCallback<Array<Uri>>,
                    fileChooserParams: FileChooserParams
                ): Boolean { // Double check that we don't have any existing callbacks
                    if (mFilePathCallback != null) {
                        mFilePathCallback!!.onReceiveValue(null)
                    }
                    mFilePathCallback = filePath
                    var takePictureIntent: Intent? = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    if (takePictureIntent!!.resolveActivity(packageManager) != null) { // Create the File where the photo should go
                        var photoFile: File? = null
                        try {
                            photoFile = createImageFile()
                            takePictureIntent.putExtra("PhotoPath", mCameraPhotoPath)
                        } catch (ex: IOException) { // Error occurred while creating the File
                            Log.e(TAG, "Unable to create Image File", ex)
                        }
                        // Continue only if the File was successfully created
                        if (photoFile != null) {
                            mCameraPhotoPath = "file:" + photoFile.absolutePath
                            takePictureIntent.putExtra(
                                MediaStore.EXTRA_OUTPUT,
                                Uri.fromFile(photoFile)
                            )
                        } else {
                            takePictureIntent = null
                        }
                    }
                    val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
                    contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
                    contentSelectionIntent.type = "image/*"
                    val intentArray: Array<Intent?>
                    intentArray = takePictureIntent.let { arrayOf(it) }
                    val chooserIntent = Intent(Intent.ACTION_CHOOSER)
                    chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
                    chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser")
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
                    startActivityForResult(chooserIntent, INPUT_FILE_REQUEST_CODE)
                    return true
                }

                fun openFileChooser(
                    uploadMsg: ValueCallback<Uri?>?,
                    acceptType: String?
                ) {
                    mUploadMessage = uploadMsg
                    // Create AndroidExampleFolder at sdcard
// Create AndroidExampleFolder at sdcard
                    val imageStorageDir = File(
                        Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES
                        )
                        , "AndroidExampleFolder"
                    )
                    if (!imageStorageDir.exists()) { // Create AndroidExampleFolder at sdcard
                        imageStorageDir.mkdirs()
                    }
                    // Create camera captured image file path and name
                    val file = File(
                        imageStorageDir.toString() + File.separator + "IMG_"
                                + System.currentTimeMillis().toString() + ".jpg"
                    )
                    mCapturedImageURI = Uri.fromFile(file)
                    // Camera capture image intent
                    val captureIntent = Intent(
                        MediaStore.ACTION_IMAGE_CAPTURE
                    )
                    captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI)
                    val i = Intent(Intent.ACTION_GET_CONTENT)
                    i.addCategory(Intent.CATEGORY_OPENABLE)
                    i.type = "image/*"
                    // Create file chooser intent
                    val chooserIntent = Intent.createChooser(i, "Image Chooser")
                    // Set camera intent to file chooser
                    chooserIntent.putExtra(
                        Intent.EXTRA_INITIAL_INTENTS
                        , arrayOf<Parcelable>(captureIntent)
                    )
                    // On select image call onActivityResult method of activity
                    startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE)
                }

                fun openFileChooser(
                    uploadMsg: ValueCallback<Uri?>?,
                    acceptType: String?,
                    capture: String?
                ) {
                    openFileChooser(uploadMsg, acceptType)
                }
            }



            if (Build.VERSION.SDK_INT >= 19) {
                webview?.setLayerType(View.LAYER_TYPE_HARDWARE, null)
            } else {
                webview?.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
            }

            webview.loadUrl(url)
            if (savedInstanceState == null) {
                webview.post {
                    Runnable {
                        webview.loadUrl(url)
                    }
                }
            }


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode != INPUT_FILE_REQUEST_CODE || mFilePathCallback == null) {
                super.onActivityResult(requestCode, resultCode, data)
                return
            }
            var results: Array<Uri>? = null
            // Check that the response is a good one
            if (resultCode == Activity.RESULT_OK) {
                if (data == null) { // If there is not data, then we may have taken a photo
                    if (mCameraPhotoPath != null) {
                        results = arrayOf(Uri.parse(mCameraPhotoPath))
                    }
                } else {
                    val dataString = data.dataString
                    if (dataString != null) {
                        results = arrayOf(Uri.parse(dataString))
                    }
                }
            }
            mFilePathCallback!!.onReceiveValue(results)
            mFilePathCallback = null
        } else if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            if (requestCode != FILECHOOSER_RESULTCODE || mUploadMessage == null) {
                super.onActivityResult(requestCode, resultCode, data)
                return
            }
            if (requestCode == FILECHOOSER_RESULTCODE) {
                if (null == mUploadMessage) {
                    return
                }
                var result: Uri? = null
                try {
                    result = if (resultCode != Activity.RESULT_OK) {
                        null
                    } else { // retrieve from the private variable if the intent is null
                        if (data == null) mCapturedImageURI else data.data
                    }
                } catch (e: Exception) {
                    Toast.makeText(
                        applicationContext, "activity :$e",
                        Toast.LENGTH_LONG
                    ).show()
                }
                mUploadMessage!!.onReceiveValue(result)
                mUploadMessage = null
            }
        }
        return
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        webview.saveState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        webview.restoreState(savedInstanceState)
    }

    inner class Callback : WebViewClient() {

        override fun onReceivedError(
            view: WebView,
            errorCode: Int,
            description: String,
            failingUrl: String
        ) {
        }

    }

    override fun onBackPressed() {
        if (webview.canGoBack()) {
            webview.goBack()
        } else {
            super.onBackPressed()
        }
    }

}
