package com.example.webviewtaskapplication.ui.xoactivity

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.webviewtaskapplication.R

class XOActivity : AppCompatActivity() {

    companion object {
        fun goToXOActivity(context: Context) {
            context.startActivity(Intent(context, XOActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_x_o)
    }

}
