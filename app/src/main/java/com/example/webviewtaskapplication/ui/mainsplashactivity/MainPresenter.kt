package com.example.webviewtaskapplication.ui.mainsplashactivity

import androidx.lifecycle.MutableLiveData
import com.example.webviewtaskapplication.data.MainRepository


class MainPresenter(private val repository: MainRepository) {

    fun forceDataRequest() {
        repository.makeResponse()
    }

    fun getUrlValue(): MutableLiveData<String> {
        return repository.getUrlValue()
    }

    fun onDestroy() {
        repository.onDestroy()
    }
}