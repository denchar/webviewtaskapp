package com.example.webviewtaskapplication.ui.mainsplashactivity

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.webviewtaskapplication.ConnectionService
import com.example.webviewtaskapplication.R
import com.example.webviewtaskapplication.ui.webviewactivity.WebViewActivity
import com.example.webviewtaskapplication.ui.xoactivity.XOActivity
import org.koin.android.ext.android.inject


class MainActivity : AppCompatActivity() {

    private val presenter: MainPresenter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (ConnectionService.isConnect(this)) {
            setupBindings()
        } else {

            onCreateDialog()
        }

    }

    private fun onCreateDialog(): Dialog {

        val builder = AlertDialog.Builder(this)

        with(builder)
        {
            setMessage("Нет подключения к интернету. Подключитесь и перезапустите приложение")
            setPositiveButton("OK") { _, _ ->
                finish()
            }
            show()
        }
        return builder.create()

    }


    private fun setupBindings() {
        presenter.forceDataRequest()
        presenter.getUrlValue().observe(this, Observer<String> {
            Log.d("Main", it.toString())
            if (it == "no") {
                goToXOActivity()
                finish()
            } else {
                goToWebViewActivity()
                finish()
            }
        })

    }


    private fun goToWebViewActivity() {
        WebViewActivity.goToWebViewActivity(this)
    }

    private fun goToXOActivity() {
        XOActivity.goToXOActivity(this)
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }
}
