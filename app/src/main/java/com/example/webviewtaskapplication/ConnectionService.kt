package com.example.webviewtaskapplication

import android.content.Context
import android.net.ConnectivityManager

class ConnectionService {

    companion object {
         fun isConnect(context: Context): Boolean {
            var network = false
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = cm.activeNetworkInfo
            if (netInfo != null && netInfo.isConnected) {
                network = true
            }
            return network
        }
    }
}