package com.example.webviewtaskapplication.data

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.webviewtaskapplication.retrofit.ApiInterface
import com.example.webviewtaskapplication.retrofit.Client
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainRepository {
    private val tag = "MainRepository"
    private var urlValue = MutableLiveData<String>()
    private lateinit var api: ApiInterface
    private val compositeDisposable = CompositeDisposable()


    fun getUrlValue(): MutableLiveData<String> {
        return urlValue
    }

     fun makeResponse() {
        api = Client.getApiRetrofitClient()!!.create(ApiInterface::class.java)
        compositeDisposable.add(
            api.getPictureOfDay()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { urlValue.value = it.url },
                    { Log.d(tag, it.message) },
                    { Log.d(tag, "Complete") })
        )
    }

    fun onDestroy() {
        compositeDisposable.clear()
    }
}