package com.example.webviewtaskapplication.di

import com.example.webviewtaskapplication.data.MainRepository
import com.example.webviewtaskapplication.retrofit.ApiInterface
import com.example.webviewtaskapplication.retrofit.Client
import com.example.webviewtaskapplication.ui.mainsplashactivity.MainPresenter
import io.reactivex.schedulers.Schedulers.single
import org.koin.dsl.module

object DependencyModule {
    val mainModule = module {
        single { Client.getApiRetrofitClient()?.create(ApiInterface::class.java) }
        single { MainRepository() }
        single { MainPresenter(get()) }
    }
}